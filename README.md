# CTG Project

Program developed in order to test the performance of heuristics for weighted graph edge coloring, which minimizes the
sum of the heaviest edges in each color.

## Installation


```bash
pip install -r requirements.txt
```

## Usage

```python
    main.py --input_graph=FILE \
    --output_path=FILE \
    [--coloring_type=(greedy | connected_sequential | largest_first)] \
    [--visualise]

Options:
    -h --help                                                           Show help.
    -V --version                                                        Show version.
    --visualise                                                         Add output graph visualisation.
    --input_graph=FILE                                                  Path to the input graph file. Graph format is analogous
                                                                        to the example given in graph_example.json file
    --output_path=FILE                                                  Path to the output graph file.
    --coloring_type=(greedy | connected_sequential | largest_first)     Coloring method type [default: largest_first].
```

## Authors
Jan Mycka, Michał Urawski