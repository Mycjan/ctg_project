import json
from networkx.classes.graph import Graph
from networkx.readwrite.json_graph import node_link_graph, node_link_data

class GraphFileConverter:
    def read_graph(self, input_file: str) -> Graph:
        with open(input_file, 'r') as graph_file:
            data = json.load(graph_file)
            return node_link_graph(data, multigraph = False)
    
    def write_graph(self, G: Graph, output_path: str):
        data = node_link_data(G)
        del data['graph']
        del data['directed']
        del data['multigraph']

        with open(output_path, 'w') as outfile:
            json.dump(data, outfile)