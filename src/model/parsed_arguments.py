from dataclasses import dataclass
from .coloring_type import ColoringType

@dataclass
class ParsedArguments: 
    input_graph: str
    output_path: str
    coloring_method: ColoringType
    visualise: bool