
from .coloring_implementation_factory import ColoringImplementationFactory
from .model.parsed_arguments import ParsedArguments
from .graph_file_converter import GraphFileConverter
from .graph_visualiser import GraphVisualiser

class AppController:

    def run_app(self, parsed_arguments: ParsedArguments):
        graph_file_converter = GraphFileConverter()
        input_graph = graph_file_converter.read_graph(parsed_arguments.input_graph)

        coloring_implementation_factory = ColoringImplementationFactory()
        coloring = coloring_implementation_factory.get_coloring(parsed_arguments.coloring_method)
        output_graph = coloring.color_graph(input_graph)  

        graph_file_converter.write_graph(output_graph, parsed_arguments.output_path)

        if parsed_arguments.visualise is True:
            graph_visualiser = GraphVisualiser()
            graph_visualiser.visualise_graph(output_graph, parsed_arguments.coloring_method.name)