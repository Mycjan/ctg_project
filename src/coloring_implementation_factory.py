from .model.coloring_type import ColoringType
from .coloring.coloring_implementation import ColoringImplementation
from .coloring.greedy_coloring_implementation import GreedyColoringImplementation
from .coloring.largest_first_coloring_implementation import LargestFirstColoringImplementation
from .coloring.connected_sequential_coloring_implementation import ConnectedSequentialColoringImplementation

class ColoringImplementationFactory:
    def get_coloring(self, coloring_method: ColoringType) -> ColoringImplementation:
        if coloring_method is ColoringType.GREEDY:
            return GreedyColoringImplementation()
        if coloring_method is ColoringType.LARGEST_FIRST:
            return LargestFirstColoringImplementation()
        if coloring_method is ColoringType.CONNECTED_SEQUENTIAL:
            return ConnectedSequentialColoringImplementation()

        raise ValueError('No method returned from ColoringImplementationFactory!')