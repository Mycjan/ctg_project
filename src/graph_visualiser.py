from networkx.classes.graph import Graph
import networkx as nx
import matplotlib.pyplot as plt

from src.coloring.helpers import get_heaviest_edges_sum

class GraphVisualiser:
    def visualise_graph(self, G: Graph, title: str = ''):
        label_pos = (0.05, -0.1)
        pos = nx.spring_layout(G)            
        edge_labels = self.get_edge_labels(G)
        
        f = plt.figure()
        ax = f.add_subplot(111)
        nx.draw_networkx_edge_labels(G, pos, edge_labels = edge_labels)
        nx.draw(G, pos, labels={node: node for node in G.nodes()}, font_color = 'white')
        plt.text(label_pos[0], label_pos[1], f'Heaviest edges sum: {get_heaviest_edges_sum(G)}', 
                ha='center', va='center', transform=ax.transAxes)
        plt.title(title)
        plt.show()

    def get_edge_labels(self, G: Graph) -> dict:
        weights = nx.get_edge_attributes(G, 'weight')
        colors = nx.get_edge_attributes(G, 'color')

        return {
            weight[0]: f'w: {weight[1]}, c: {color[1]}' for weight, color in zip(weights.items(), colors.items())
        }