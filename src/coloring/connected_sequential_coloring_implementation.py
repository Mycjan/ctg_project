from .coloring_implementation import ColoringImplementation
from networkx.classes.graph import Graph
from .helpers import minimum_color_for_edge


class ConnectedSequentialColoringImplementation(ColoringImplementation):

    def color_graph(self, graph: Graph) -> Graph:
        g = graph.copy()
        edges = g.edges(data=True)
        heaviest_edge = max(edges, key=lambda e: e[2]['weight'])
        heaviest_edge[2]['color'] = minimum_color_for_edge(heaviest_edge[0], heaviest_edge[1], g)
        connected_edges = self.__get_connected_not_colored_edges(heaviest_edge[0], heaviest_edge[1], g)

        while len(connected_edges) != 0:
            he = max(connected_edges,
                     key=lambda e: g.get_edge_data(e[0], e[1])['weight'])
            g.get_edge_data(he[0], he[1])['color'] = minimum_color_for_edge(he[0], he[1], g)
            connected_edges.remove(he)

            connected_edges = list(set([i for i in connected_edges +
                                        self.__get_connected_not_colored_edges(he[0], he[1], g)]))

        return g

    @staticmethod
    def __get_connected_not_colored_edges(u, v, graph) -> list:
        edges_u = [tuple(sorted((u, n))) for n in list(graph.adj[u])
                   if 'color' not in graph.get_edge_data(u, n)]
        edges_v = [tuple(sorted((v, n))) for n in list(graph.adj[v])
                   if 'color' not in graph.get_edge_data(v, n)]

        return list(set([i for i in edges_u + edges_v]))

