from abc import ABC, abstractmethod
from networkx.classes.graph import Graph

class ColoringImplementation(ABC):
    
    @abstractmethod
    def color_graph(self, graph: Graph) -> Graph:
        pass