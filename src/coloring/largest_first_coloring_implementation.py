from .coloring_implementation import ColoringImplementation
from networkx.classes.graph import Graph
from .helpers import minimum_color_for_edge

class LargestFirstColoringImplementation(ColoringImplementation):

    def color_graph(self, graph: Graph) -> Graph:
        g = graph.copy()
        nodes = list(graph.nodes)
        while len(nodes) > 1:
            nodes_weights = self.__get_nodes_uncolored_weight(nodes, g)
            idx_to_remove = list()
            for i, weight in enumerate(nodes_weights):
                if weight == 0:
                    idx_to_remove.append(i)
            idx_to_remove = sorted(idx_to_remove, reverse=True)
            for idx in idx_to_remove:
                nodes_weights.pop(idx)
                nodes.pop(idx)

            max_node_idx = max(zip(nodes_weights, range(len(nodes_weights))))[1]
            node = nodes[max_node_idx]
            edges_to_color = g.edges(node, data=True)
            edges_to_color = sorted(edges_to_color, key=lambda e: e[2]['weight'], reverse=True)
            for u, v, edge_data in edges_to_color:
                if 'color' not in edge_data:
                    edge_data['color'] = minimum_color_for_edge(u, v, g)
            nodes.remove(node)

        return g

    @staticmethod
    def __get_nodes_uncolored_weight(nodes, graph: Graph) -> list:
        weights = list()
        for node in nodes:
            neighbours = list(graph.adj[node])
            weight = 0
            count = 0
            for neighbour in neighbours:
                edge_data = graph.get_edge_data(node, neighbour)
                if 'color' not in edge_data:
                    weight += edge_data['weight']
                    count += 1

            if count != 0:
                weight = weight / count
            weights.append(weight)

        return weights
