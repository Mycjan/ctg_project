from networkx.classes.graph import Graph


def minimum_color_for_edge(u, v, graph: Graph) -> int:
    used_colors_u = colors_from_node(u, graph)
    used_colors_v = colors_from_node(v, graph)

    used_colors = used_colors_u.union(used_colors_v)
    color = 0
    while color in used_colors:
        color += 1

    return color


def colors_from_node(u, graph: Graph) -> set:
    colors = set()
    u_neighbours = list(graph.adj[u])
    for n in u_neighbours:
        edge_data = graph.get_edge_data(u, n)

        if 'color' in edge_data is not None and not (edge_data['color'] in colors):
            colors.add(edge_data['color'])

    return colors


def get_heaviest_edges_sum(graph: Graph) -> int:
    edges = graph.edges(data=True)
    colors_dict = {}
    for u, v, edge_data in edges:
        color = edge_data['color']
        weight = edge_data['weight']
        if color in colors_dict and colors_dict[color] < weight:
            colors_dict[color] = weight
        if color not in colors_dict:
            colors_dict[color] = weight

    return sum(colors_dict.values())

def get_max_color(graph: Graph) -> int:
    colors = [x[2]['color'] for x in graph.edges(data = True)]
    return max(colors) + 1

def get_max_degree(graph: Graph) -> int:
    return max([x[1] for x in graph.degree])