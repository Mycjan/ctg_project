from .coloring_implementation import ColoringImplementation
from networkx.classes.graph import Graph
from .helpers import minimum_color_for_edge


class GreedyColoringImplementation(ColoringImplementation):

    def color_graph(self, graph: Graph) -> Graph:
        g = graph.copy()
        edges = g.edges(data=True)
        sorted_edges = sorted(edges, key=lambda e: e[2]['weight'], reverse=True)
        for u, v, edge_data in sorted_edges:
            edge_data['color'] = minimum_color_for_edge(u, v, g)

        return g
