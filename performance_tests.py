from networkx import Graph
import networkx as nx
import random
import time
import pandas as pd


from src.coloring.connected_sequential_coloring_implementation import ConnectedSequentialColoringImplementation
from src.coloring.greedy_coloring_implementation import GreedyColoringImplementation
from src.coloring.largest_first_coloring_implementation import LargestFirstColoringImplementation
from src.coloring.helpers import get_heaviest_edges_sum, get_max_color, get_max_degree


SEED = 1231412

def set_graph_weights(g: Graph, max_weight: int) -> Graph:
    g_copy = g.copy()

    for u, v, attr in g_copy.edges(data=True):
        g_copy[u][v]['weight'] = random.randint(1, max_weight)

    return g_copy

def create_tree():
    num_vertices = 5000
    max_weight = 10000
    tree = nx.random_tree(num_vertices, seed=SEED)

    tree = set_graph_weights(tree, max_weight)
    
    return (tree, 'Tree')

def create_cycle():
    num_vertices = 10000
    max_weight = 15000
    cycle = nx.cycle_graph(num_vertices)
    cycle = set_graph_weights(cycle, max_weight)

    return (cycle, 'Cycle')

def create_complete_graph():
    num_vertices = 200
    max_weight = 1000
    complete_graph = nx.complete_graph(num_vertices)
    complete_graph = set_graph_weights(complete_graph, max_weight)

    return (complete_graph, 'Complete graph')

def create_erdos_graph():
    num_vertices = 500
    max_weight = 3000
    p = 0.2
    erdos_graph = nx.erdos_renyi_graph(num_vertices, p, seed=SEED)
    erdos_graph = set_graph_weights(erdos_graph, max_weight)

    return (erdos_graph, 'Erdos-Renyi graph')

def create_barabasi_albert_graph():
    num_vertices = 500
    max_weight = 4000
    m = 30
    barabasi_albert_graph = nx.barabasi_albert_graph(num_vertices, m, seed=SEED)
    barabasi_albert_graph = set_graph_weights(barabasi_albert_graph, max_weight)

    return (barabasi_albert_graph, 'Barabasi-Albert graph')

def create_tree_2():
    num_vertices = 4000
    max_weight = 1000
    tree = nx.random_tree(num_vertices, seed=SEED)

    tree = set_graph_weights(tree, max_weight)
    return (tree, 'Tree 2')

def create_complete_graph_2():
    num_vertices = 150
    max_weight = 40
    complete_graph = nx.complete_graph(num_vertices)
    complete_graph = set_graph_weights(complete_graph, max_weight)

    return (complete_graph, 'Complete graph 2')

def create_erdos_graph_2():
    num_vertices = 900
    max_weight = 200
    p = 0.1
    erdos_graph = nx.erdos_renyi_graph(num_vertices, p, seed=SEED)
    erdos_graph = set_graph_weights(erdos_graph, max_weight)

    return (erdos_graph, 'Erdos-Renyi graph 2')

if __name__ == "__main__":
    random.seed(SEED)

    methods = [
        (ConnectedSequentialColoringImplementation(), 'Connected sequential'),
        (GreedyColoringImplementation(), 'Greedy'),
        (LargestFirstColoringImplementation(), 'Largest fist')
    ]
    graphs = [
        create_tree(),
        create_cycle(),
        create_complete_graph(),
        create_erdos_graph(),
        create_barabasi_albert_graph(),
        create_tree_2(),
        create_complete_graph_2(),
        create_erdos_graph_2()
    ]

    columns = ['typ grafu', 'liczba wierzchołków', 'liczba krawędzi', 'metoda kolorowania', 'suma najcięższych krawędzi',
            'liczba kolorów', 'czas wykonania']
    df = pd.DataFrame(columns = columns)

    for (method, method_name) in methods:
        print(f'{method_name} coloring: ')
        print('-------------------')
        for (g, graph_name) in graphs:
            print(f'{graph_name}:')
            start_time = time.perf_counter()
            colored = method.color_graph(g)
            performance_time = time.perf_counter() - start_time
            edges_sum = get_heaviest_edges_sum(colored)
            max_color = get_max_color(colored)
            max_degree = get_max_degree(colored)
            no_vertices = len(colored)
            no_edges = colored.size()

            df2 = pd.DataFrame([[graph_name, no_vertices, no_edges, method_name, edges_sum, max_color, performance_time]],
            columns=columns)
            df = pd.concat([df, df2]).reset_index(drop=True)

            print(f'- Elapsed time: {performance_time}')
            print(f'- Heaviest edges sum: {edges_sum}')
            print(f'- Number of colors: {max_color}')
            print(f'- Max degree: {max_degree}')
    df.to_csv('./perf_results.csv')

