import networkx as nx

from src.coloring.greedy_coloring_implementation import GreedyColoringImplementation
from src.coloring.connected_sequential_coloring_implementation import ConnectedSequentialColoringImplementation
from src.coloring.largest_first_coloring_implementation import LargestFirstColoringImplementation
from src.coloring.helpers import get_heaviest_edges_sum
from src.graph_visualiser import GraphVisualiser



def test_k4_greedy():
    g = get_k4()
    g = GreedyColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 115
    assert is_correct_coloring(g) == True, "K4 greedy correct coloring"
    assert g[0][1]['color'] == 0, "K4 color check"
    assert g[0][2]['color'] == 2, "K4 color check"
    assert g[0][3]['color'] == 1, "K4 color check"
    assert g[1][2]['color'] == 1, "K4 color check"
    assert g[1][3]['color'] == 2, "K4 color check"
    assert g[2][3]['color'] == 0, "K4 color check"
    return g


def test_k4_cs():
    g = get_k4()
    g = ConnectedSequentialColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 115
    assert is_correct_coloring(g) == True, "K4 greedy correct coloring"
    assert g[0][1]['color'] == 0, "K4 color check"
    assert g[0][2]['color'] == 2, "K4 color check"
    assert g[0][3]['color'] == 1, "K4 color check"
    assert g[1][2]['color'] == 1, "K4 color check"
    assert g[1][3]['color'] == 2, "K4 color check"
    assert g[2][3]['color'] == 0, "K4 color check"
    return g


def test_k4_lf():
    g = get_k4()
    g = LargestFirstColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 115
    assert is_correct_coloring(g) == True, "K4 greedy correct coloring"
    assert g[2][3]['color'] == 0, "K4 color check"
    assert g[1][2]['color'] == 1, "K4 color check"
    assert g[0][2]['color'] == 2, "K4 color check"
    assert g[1][3]['color'] == 2, "K4 color check"
    assert g[0][3]['color'] == 1, "K4 color check"
    assert g[0][1]['color'] == 0, "K4 color check"
    return g


def test_fish_greedy():
    g = get_fish()
    g = GreedyColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 95
    assert is_correct_coloring(g) == True, "Fish greedy correct coloring"
    assert g[0][1]['color'] == 2, "Fish color check"
    assert g[0][6]['color'] == 1, "Fish color check"
    assert g[0][7]['color'] == 3, "Fish color check"
    assert g[1][2]['color'] == 1, "Fish color check"
    assert g[1][7]['color'] == 0, "Fish color check"
    assert g[2][3]['color'] == 0, "Fish color check"
    assert g[2][4]['color'] == 3, "Fish color check"
    assert g[2][5]['color'] == 2, "Fish color check"
    assert g[3][4]['color'] == 1, "Fish color check"
    assert g[5][6]['color'] == 0, "Fish color check"
    assert g[5][7]['color'] == 1, "Fish color check"
    assert g[6][7]['color'] == 2, "Fish color check"
    return g


def test_fish_cs():
    g = get_fish()
    g = ConnectedSequentialColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 104
    assert is_correct_coloring(g) == True, "Fish greedy correct coloring"
    assert g[0][1]['color'] == 0, "Fish color check"
    assert g[0][6]['color'] == 1, "Fish color check"
    assert g[0][7]['color'] == 3, "Fish color check"
    assert g[1][2]['color'] == 1, "Fish color check"
    assert g[1][7]['color'] == 2, "Fish color check"
    assert g[2][3]['color'] == 0, "Fish color check"
    assert g[2][4]['color'] == 3, "Fish color check"
    assert g[2][5]['color'] == 2, "Fish color check"
    assert g[3][4]['color'] == 1, "Fish color check"
    assert g[5][6]['color'] == 0, "Fish color check"
    assert g[5][7]['color'] == 1, "Fish color check"
    assert g[6][7]['color'] == 4, "Fish color check"
    return g


def test_fish_lf():
    g = get_fish()
    g = LargestFirstColoringImplementation().color_graph(g)
    assert get_heaviest_edges_sum(g) == 95
    assert is_correct_coloring(g) == True, "Fish greedy correct coloring"
    assert g[0][1]['color'] == 2, "Fish color check"
    assert g[0][6]['color'] == 1, "Fish color check"
    assert g[0][7]['color'] == 3, "Fish color check"
    assert g[1][2]['color'] == 1, "Fish color check"
    assert g[1][7]['color'] == 0, "Fish color check"
    assert g[2][3]['color'] == 0, "Fish color check"
    assert g[2][4]['color'] == 3, "Fish color check"
    assert g[2][5]['color'] == 2, "Fish color check"
    assert g[3][4]['color'] == 1, "Fish color check"
    assert g[5][6]['color'] == 0, "Fish color check"
    assert g[5][7]['color'] == 1, "Fish color check"
    assert g[6][7]['color'] == 2, "Fish color check"
    return g


def is_correct_coloring(g: nx.Graph) -> bool:
    for node in g.nodes:
        colors = set()
        for (u, v, edge_data) in g.edges(node, data=True):
            c = edge_data['color']
            if c is None or c in colors:
                return False
            colors.add(c)
    return True


def get_k4() -> nx.Graph:
    k_4 = nx.complete_graph(4)
    edge_weights = {
        (0, 1): {"weight": 15},
        (0, 2): {"weight": 25},
        (0, 3): {"weight": 20},
        (1, 2): {"weight": 35},
        (1, 3): {"weight": 30},
        (2, 3): {"weight": 50}
    }
    nx.set_edge_attributes(k_4, edge_weights)
    return k_4


def get_fish() -> nx.Graph:
    fish = nx.Graph()
    fish.add_nodes_from(range(0, 8))
    fish.add_edges_from(
        [(0, 1), (0, 6), (0, 7), (1, 2), (1, 7), (2, 3), (2, 4), (2, 5), (3, 4), (5, 6), (5, 7), (6, 7)])
    edge_weights = {
        (0, 1): {"weight": 15},
        (0, 6): {"weight": 30},
        (0, 7): {"weight": 10},
        (1, 2): {"weight": 23},
        (1, 7): {"weight": 18},
        (2, 3): {"weight": 35},
        (2, 4): {"weight": 7},
        (2, 5): {"weight": 9},
        (3, 4): {"weight": 25},
        (5, 6): {"weight": 40},
        (5, 7): {"weight": 12},
        (6, 7): {"weight": 6}
    }
    nx.set_edge_attributes(fish, edge_weights)
    return fish


if __name__ == "__main__":
    visualize = False
    graphs = []
    graphs.append((test_k4_greedy(), 'greedy'))
    graphs.append((test_k4_cs(), 'connected_sequential'))
    graphs.append((test_k4_lf(), 'largest_first'))
    graphs.append((test_fish_greedy(), 'greedy'))
    graphs.append((test_fish_cs(), 'connected_sequential'))
    graphs.append((test_fish_lf(), 'largest_first'))

    if visualize:
        visualiser = GraphVisualiser()
        for g in graphs:
            visualiser.visualise_graph(g[0], g[1])
    print("Tests passed")
