"""
Program developed in order to test the performance of heuristics for weighted graph edge coloring, which minimizes the
sum of the heaviest edges in each color.

Usage:
    main.py --input_graph=FILE \
    --output_path=FILE \
    [--coloring_type=(greedy | connected_sequential | largest_first)] \
    [--visualise]

Options:
    -h --help                                                           Show help.
    -V --version                                                        Show version.
    --visualise                                                         Add output graph visualisation.
    --input_graph=FILE                                                  Path to the input graph file. Graph format is analogous
                                                                        to the example given in graph_example.json file
    --output_path=FILE                                                  Path to the output graph file.
    --coloring_type=(greedy | connected_sequential | largest_first)     Coloring method type [default: largest_first].
"""

from docopt import docopt
from src.model.coloring_type import ColoringType
from src.model.parsed_arguments import ParsedArguments
from src.app_controller import AppController

__version__ = '0.0.1'

if __name__ == '__main__':
    args = docopt(__doc__)
    parsed_args = ParsedArguments(
        input_graph = args['--input_graph'],
        output_path = args['--output_path'],
        coloring_method = ColoringType(args['--coloring_type']),
        visualise = args['--visualise']
    )
    app_controller = AppController()
    app_controller.run_app(parsed_args)
